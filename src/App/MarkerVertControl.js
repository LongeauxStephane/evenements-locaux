import app from './App';

export class MarkerVertControl {
    onAdd( map ) {
        this._map = map;

        this._container = document.createElement('div');
        this._container.classList.add( 'mapboxgl-ctrl', 'ctrl-current-marker', 'mrk-vert' );
        this._container.textContent = 'V';
        this._container.addEventListener( 'click', this.onControlClick.bind( this ) );

        return this._container;
    }

    onRemove() {
        this._container.parentNode.removeChild(this._container);
        this._map = undefined;
    }

    onControlClick( evt ) {
        const isVert = this._container.classList.contains( 'mrk-vert' );

        // Si le marker est déjà sur la carte
        if( isVert ) {
            this._container.classList.remove( 'mrk-vert' );
            app.markerVert.remove();

            return;
        }

        // Si le marker n'est plus sur carte
        this._container.classList.add( 'mrk-vert' );
        app.markerVert.addTo( this._map );
    }
}