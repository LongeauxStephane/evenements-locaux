import app from './App';

export class CurrentPosMarkerControl {
    onAdd( map ) {
        this._map = map;

        this._container = document.createElement('div');
        this._container.classList.add( 'mapboxgl-ctrl', 'ctrl-current-marker', 'active' );
        this._container.textContent = '🧿';
        this._container.addEventListener( 'click', this.onControlClick.bind( this ) );

        return this._container;
    }

    onRemove() {
        this._container.parentNode.removeChild(this._container);
        this._map = undefined;
    }

    onControlClick( evt ) {
        const isActive = this._container.classList.contains( 'active' );

        // Si le marker est déjà sur la carte
        if( isActive ) {
            this._container.classList.remove( 'active' );
            app.currentPosMarker.remove();

            return;
        }

        // Si le marker n'est plus sur carte
        this._container.classList.add( 'active' );
        app.currentPosMarker.addTo( this._map );
    }
}