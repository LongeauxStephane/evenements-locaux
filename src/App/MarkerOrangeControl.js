import app from './App';

export class MarkerOrangeControl {
    onAdd( map ) {
        this._map = map;

        this._container = document.createElement('div');
        this._container.classList.add( 'mapboxgl-ctrl', 'ctrl-current-marker', 'mrk-orange' );
        this._container.textContent = 'O';
        this._container.addEventListener( 'click', this.onControlClick.bind( this ) );

        return this._container;
    }

    onRemove() {
        this._container.parentNode.removeChild(this._container);
        this._map = undefined;
    }

    onControlClick( evt ) {
        const isRouge = this._container.classList.contains( 'mrk-orange' );

        // Si le marker est déjà sur la carte
        if( isRouge ) {
            console.log("App: " + app);
            console.log("Rouge: " + app.markerOrange);
            this._container.classList.remove( 'mrk-orange' );
            app.markerOrange.remove();

            return;
        }

        // Si le marker n'est plus sur carte
        this._container.classList.add( 'mrk-orange' );
        app.markerOrange.addTo( this._map );
    }
}