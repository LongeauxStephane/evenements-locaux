import app from './App';

export class RefreshControl {
    onAdd( map ) {
        this._map = map;

        this._container = document.createElement('div');
        this._container.classList.add( 'mapboxgl-ctrl', 'ctrl-current-marker', 'btn-refresh' );
        this._container.textContent = '⟳';
        this._container.addEventListener( 'click', this.onControlClick.bind( this ) );

        return this._container;
    }

    onRemove() {
        this._container.parentNode.removeChild(this._container);
        this._map = undefined;
    }

    onControlClick( evt ) {
        const isRefreshed = this._container.classList.contains( 'btn-refresh' );

        // Si le marker est déjà sur la carte
        if( isRefreshed ) {
            this._container.classList.remove( 'btn-refresh' );
            app.markerRouge.remove();
            app.markerOrange.remove();
            app.renderMarkers();

            return;
        }

        // Si le marker n'est plus sur carte
        this._container.classList.add( 'btn-refresh' );
        app.btnRefresh.addTo( this._map );
    }
}