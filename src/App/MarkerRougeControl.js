import app from './App';

export class MarkerRougeControl {
    onAdd( map ) {
        this._map = map;

        this._container = document.createElement('div');
        this._container.classList.add( 'mapboxgl-ctrl', 'ctrl-current-marker', 'mrk-rouge' );
        this._container.textContent = 'R';
        this._container.addEventListener( 'click', this.onControlClick.bind( this ) );

        return this._container;
    }

    onRemove() {
        this._container.parentNode.removeChild(this._container);
        this._map = undefined;
    }

    onControlClick( evt ) {
        const isRouge = this._container.classList.contains( 'mrk-rouge' );

        // Si le marker est déjà sur la carte
        if( isRouge ) {
            console.log("App: " + app);
            console.log("Rouge: " + app.markerRouge);
            this._container.classList.remove( 'mrk-rouge' );
            app.markerRouge.remove();

            return;
        }

        // Si le marker n'est plus sur carte
        this._container.classList.add( 'mrk-rouge' );
        app.markerRouge.addTo( this._map );
    }
}