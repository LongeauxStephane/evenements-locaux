import mapboxgl from "mapbox-gl";
import app from "./App";
import {Meetup} from "../Models/Meetup";



export class EventControl {

    static onCreation() {

        let btnEvent = document.getElementById('btn-event');

        btnEvent.addEventListener('click', () => {
            let evt_titre = document.getElementById("evt_titre").value;
            let evt_desc = document.getElementById("evt_desc").value;
            let evt_debut = document.getElementById("evt_debut").value;
            let evt_fin = document.getElementById("evt_fin").value;
            let evt_lat = document.getElementById("evt_lat").value;
            let evt_lng = document.getElementById("evt_lng").value;

            const eventLngLat = {
                lat: evt_lat,
                lng: evt_lng
            };

            const popup = new mapboxgl.Popup();
            popup.setText( evt_titre + 'Description: '  + '\n' + evt_desc + '\n' + evt_debut + '\n' + 'fin: ' + evt_fin );

            const eventPinMarker = new mapboxgl.Marker();
            eventPinMarker.getElement().setAttribute(
                'title',
                evt_titre + '\n' +'debut: ' + evt_debut + '\n' + 'fin: ' + evt_fin
            );
            eventPinMarker
                //.setAttribute('title', evt_titre);
                .setLngLat( eventLngLat )
                .setPopup( popup )
                .addTo( app.map );

             const meetup = new Meetup();
             meetup.title = evt_titre;
             meetup.desc = evt_desc;
             meetup.debut = evt_debut;
             meetup.fin = evt_fin;
             meetup.coords = eventLngLat;

             app.listEvents.push(meetup);
             localStorage.setItem(app.storageName,JSON.stringify(app.listEvents) );

        });
    }


    static getMeetupStatus(meetup) {
        let td = new Date();
        let evt_date = new Date ( meetup.debut );
        let threedays = 3600 * 24 * 3 * 1000;
        let interval = evt_date - td;

        console.log(interval, threedays, meetup.debut);
        console.log(evt_date);


        if ( interval > threedays ) {
            console.log('vert');
            return { code: 1, interval: interval };
        } else if (interval < 0  ) {
            console.log('rouge');
            return { code: 3, interval: interval };
        } else {
            console.log('orange');
            return { code: 2, interval: interval };
        }
    }

}




