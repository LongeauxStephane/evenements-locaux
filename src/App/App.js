import mapboxgl from 'mapbox-gl';
import 'bootstrap';

import config from '../../app.config.json';
import '../styles/styles.scss';

import { CurrentPosMarkerControl } from './CurrentPosMarkerControl';
import { MarkerVertControl } from './MarkerVertControl';
import { MarkerRougeControl } from './MarkerRougeControl';
import { MarkerOrangeControl } from './MarkerOrangeControl';
import { RefreshControl } from './RefreshControl';
import { MapStyleControl } from './MapStyleControl';

import { EventControl } from './EventControl';
import { Meetup } from '../Models/Meetup';

class App {
    map;
    refresh;
    currentPosMarker;
    markerVert;
    markerRouge;
    markerOrange;
    listEvents = [];
    get storageName() { return 'Meetup'; }

    constructor() {
        mapboxgl.accessToken = config.apis.mapbox_gl.api_key;
    }

    start() {

        // 3 - Objet LngLat de l'API Mapboxgl
        const centerMap = new mapboxgl.LngLat( 2.213749, 46.227638 );

        // Instanciation de la carte
        this.map = new mapboxgl.Map({
            container: 'map',
            style: config.apis.mapbox_gl.map_style,
            center: centerMap,
            zoom: 5.5
        });

        // Ajout des boutons de zoom
        const nav = new mapboxgl.NavigationControl({
            showCompass: false
        });
        this.map.addControl( nav, 'top-right' );

        // Ajout du controle personnalisé
        const mapStyles = new MapStyleControl();
        this.map.addControl( mapStyles, 'top-left' );

        navigator.geolocation.getCurrentPosition( this.onPositionFound.bind( this ) );

    }

    onPositionFound( geoData ) {
        const currentLngLat = {
            lat: geoData.coords.latitude,
            lng: geoData.coords.longitude
        };

        // Creation de la popup du marker
        const popup = new mapboxgl.Popup();

        // Récupération de la météo
        const url = `${config.apis.openweathermaps.base_url}appid=${config.apis.openweathermaps.api_key}&lat=${currentLngLat.lat}&lon=${currentLngLat.lng}&lang=fr&units=metric&exclude=minutely,hourly`;

        // De manière plus concise
        fetch( url )
            .then( response => response.json() )
            .then( data => popup.setText( `Il fait ${data.current.temp}°C` ) );

        // Ajout de Marker sur la carte
        this.currentPosMarker = new mapboxgl.Marker();
        this.currentPosMarker
            .setLngLat( currentLngLat )
            .setPopup( popup )
            .addTo( this.map );

        // Ajout du controle personalisé pour afficher/masquer le marker de la position courrante
        const currentPosMarkerControl = new CurrentPosMarkerControl();
        this.map.addControl( currentPosMarkerControl, 'top-right' );

        const markerRougeControl = new MarkerRougeControl();
        this.map.addControl( markerRougeControl, 'top-right' );

        const markerOrangeControl = new MarkerOrangeControl();
        this.map.addControl( markerOrangeControl, 'top-right' );

        const markerVertControl = new MarkerVertControl();
        this.map.addControl( markerVertControl, 'top-right' );

        const refreshControl = new RefreshControl();
        this.map.addControl( refreshControl, 'top-left' );

        // Avec une transition animée
        this.map.easeTo({
            center: currentLngLat,
            duration: 2000,
            zoom: 18
        });

        //Ajout de la gestion des événements
        EventControl.onCreation();

        // Récupération des données du localStorage
        const strData = localStorage.getItem( this.storageName );

        // Si la clé n'existe pas on ne doit rien traiter
        if( strData ) {
            // JSON.parse() reconvertit en JSON une chaîne issue de JSON.stringify()
            const arrJson = JSON.parse(strData);

            // On doit traiter le tableau de JSON pour en faire un tableau d'objets PostIt
            for( let json of arrJson ) {
                this.listEvents.push( Meetup.fromJSON(json) );
            }
        }

        this.renderMarkers();
    }

    renderMarkers() {
        for( let meetup of this.listEvents ) {
            const meetupStatus = EventControl.getMeetupStatus(meetup);
            const popup = new mapboxgl.Popup();


            let timeMsg = "";
            let markerColor = "#0C0";
            let date = new Date(meetupStatus.interval);


           if ( meetupStatus.code === 2 ) {
               markerColor = "#FC0";
               timeMsg = 'Attention, commence dans ' + date.getDay()  + ' jours' ;

               // Ajout de Marker sur la carte
               this.markerOrange = new mapboxgl.Marker({color: markerColor});
               this.markerOrange.getElement().setAttribute(
                   'title',
                   meetup.titre + '\n' +'debut: ' + meetup.debut + '\n' + 'fin: ' + meetup.fin
               );

               this.markerOrange
                   .setLngLat( meetup.coords )
                   .setPopup( popup )
                   .addTo( this.map );

           } else if ( meetupStatus.code === 3 ) {
               markerColor = "#C00";
               timeMsg = 'Quel dommage ! Vous avez raté cet événement !' ;

               // Ajout de Marker sur la carte
               this.markerRouge = new mapboxgl.Marker({color: markerColor});
               this.markerRouge.getElement().setAttribute(
                   'title',
                   meetup.titre + '\n' +'debut: ' + meetup.debut + '\n' + 'fin: ' + meetup.fin
               );

               this.markerRouge
                   .setLngLat( meetup.coords )
                   .setPopup( popup )
                   .addTo( this.map );

           } else if ( meetupStatus.code === 1 ) {
               markerColor = "#01D758";
               timeMsg = 'Quel dommage ! Vous avez raté cet événement !';

               // Ajout de Marker sur la carte
               this.markerVert = new mapboxgl.Marker({color: markerColor});
               this.markerVert.getElement().setAttribute(
                   'title',
                   meetup.titre + '\n' + 'debut: ' + meetup.debut + '\n' + 'fin: ' + meetup.fin
               );

               this.markerVert
                   .setLngLat(meetup.coords)
                   .setPopup(popup)
                   .addTo(this.map);
           }

            popup.setHTML(
                timeMsg + '<br/>' +
                "<h4>" + meetup.title + "</h4>"  + '<br/>' +
                "<strong>" + 'Description: '  + "</strong>" + '<br/>' +
                meetup.desc + '<br/>' +
                "<strong>" + 'Début: ' + "</strong>" + Date(meetup.debut) + '<br/>' +
                "<strong>" + 'Fin: '  + "</strong>" + Date(meetup.fin)
            );


            //
            // const marker = new mapboxgl.Marker({color: markerColor});
            // marker.getElement().setAttribute(
            //     'title',
            //     meetup.titre + '\n' +'debut: ' + meetup.debut + '\n' + 'fin: ' + meetup.fin
            // );
            //
            // marker
            //     .setLngLat( meetup.coords )
            //     .setPopup( popup )
            //     .addTo( this.map );

        }
    }

}

const app = new App();

export default app;