// Controle utilisateur personnalisé pour Mapbox
// Voir doc: https://docs.mapbox.com/mapbox-gl-js/api/markers/#icontrol
// La doc donne les spécifications de l'"interface" de la classe
export class MapStyleControl {
    onAdd( map ) {
        this._map = map;

        this._container = document.createElement('div');
        this._container.classList.add( 'mapboxgl-ctrl' );
        this._container.addEventListener( 'click', this.onControlClick.bind( this ) );

        return this._container;
    }
        
    onRemove() {
        this._container.parentNode.removeChild(this._container);
        this._map = undefined;
    }

    onControlClick( evt ) {
        console.log( this );
    }
}